## DSKMS介绍
DSKMS系统是基于ThinkPhp6.0+Vue开发的一套完善的内容(视频\文章)付费系统,功能包含 外加打赏、限时特价、 店铺会员可以免费听所有课程、以及分销功能

## 导航栏目
 [帮助手册](http://www.csdeshang.com/home/help/index/id/143.html)
 | [功能清单](http://www.csdeshang.com/home/dskms/feature.html)
 | [官网地址](http://www.csdeshang.com)
 | [TP6开发手册](https://www.kancloud.cn/manual/thinkphp6_0/1037479)
 | [Vue.js手册](https://cn.vuejs.org/)

## QQ交流群
DSKMS开源商城官方群:553212556  <a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=792e214cbbda512d80ffcaf306f0f92bc69f21ef2bd8096e0d52c733b8e5499a"><img border="0" src="//pub.idqqimg.com/wpa/images/group.png" alt="DSCMS开源官方群2" title="DSCMS开源官方群2"></a>

## 演示Demo
| 演示角色  | 演示地址                                | 账号 | 账号 |
|-------|-------------------------------------|----|----|
| 后台PC端 | https://dskms.csdeshang.com/admin/ |  admin  |  admin888  |
| 用户PC端 | https://dskms.csdeshang.com/ |  buyer  |  123456  |
| 机构PC端 | https://dskms.csdeshang.com/home/sellerlogin/login.html |  seller  |  123456  |
| 用户手机端 | https://m.dskms.csdeshang.com/ |  buyer  |  123456  |
| 机构手机端 | https://m.dskms.csdeshang.com/home/sellerlogin |  seller  |  123456  |

## 技术评价
1. B/S架构
2. MVC编码架构，采用Thinkphp6.0框架
3. 支持Compser
4. 支持阿里云存储
5. 支持负载均衡
6. 支持Mysql读写分离
7. 支持Redis/Memcached
8. 支持Linux/Unix/Windows服务器，支持Apache/IIS/Nginx等

## 系统功能
1. 设置：站点设置、账号同步、上传设置、SEO设置、邮箱短信、支付方式(支付宝/微信/银联)、权限设置、地区管理、数据备份、操作日志
2. 会员：会员管理、会员级别、经验值管理、会员通知、积分管理、预存款
3. 课程：课程分类、课程管理、空间管理
4. 机构：机构管理、机构资金、机构保证金、机构等级、机构分类、机构帮助、自营机构
5. 交易：订单管理、退款管理、订单结算、咨询管理、评价管理、结算管理
6. 网站：文章分类、文章管理、会员协议、导航管理、广告管理、友情链接、平台客服
7. 营销：分销管理、代金券管理、兑换礼品、平台充值卡
8. 问卷考试：题库分类、题库管理、试卷管理、考试记录、批阅试卷
9. 公众号：公众号配置、微信菜单、关键字回复、绑定列表、消息推送
10. 直播：直播设置、直播申请、直播聊天

## 相关依赖SDK安装
1. 多应用模式扩展     composer require topthink/think-multi-app
2. think-view        composer require topthink/think-view
3. think-captcha     composer require topthink/think-captcha
4. think-image       composer require topthink/think-image
5. thinkphp-jump     composer require liliuwei/thinkphp-jump
5. 阿里云OSS         composer require aliyuncs/oss-sdk-php   
介绍地址：https://help.aliyun.com/document_detail/32099.html?spm=5176.87240.400427.47.eaLg1R
6. phpmailer         composer require phpmailer/phpmailer
7. 阿里云短信         composer require alibabacloud/client
8. 腾讯云短信         composer require qcloudsms/qcloudsms_php
9. 签名工具           composer require firebase/php-jwt
10. 腾讯云点播        composer require tencentcloud/tencentcloud-sdk-php
安装tencentcloud  PHP 5.6.33 版本及以上。https://github.com/tencentcloud/tencentcloud-sdk-php
11. 进入GatewayWorker子目录安装composer install
12. 安装GatewayClient   composer require workerman/gatewayclient
13. 安装阿里云点播       composer require alibabacloud/sdk


## APIDOC 生成API
apidoc -i application/api/controller -o public/apidoc/

## 更新日志
#### V3.0.2
DSKms免费版更新 
1. 虚拟退款优化流程
2. 优化后台部分页面显示
3. 优化未配置短信时报错
4. 视频播放器引用腾讯视频播放器
5. 店铺入驻可设置（仅个人入驻，仅企业入驻，全部都可以，全部都不可以）
6. 修复后台添加管理员密码可以为空的问题
7. 新增课程上传的时候，在视频链接下方显示上传视频按钮以及查看视频库按钮

授权版更新
1. 视频播放器引用腾讯视频播放器
2. 修改会员登录，手机号码登录只能输入11位数字
3. 部分页面优化
4. 店铺入驻可设置（仅个人入驻，仅企业入驻，全部都可以，全部都不可以）
5. 新增课程上传的时候，在视频链接下方显示上传视频按钮以及查看视频库按钮

#### V3.0.1
Thinkphp由TP5.0.24升级为TP6.0

#### V2.2.2 
DSKms免费版更新 

1. 意见反馈面包屑导航优化
2. 优化结算
3. 关闭的机构不显示
4. 优化邮件显示
5. 新增机构登录验证码
6. 新增后台 添加和编辑礼品页面 删除编辑器图片的成功提示
7. 修改前端机构首页和商品详情页面左侧机构地图使用经纬度定位，使定位更准确
8. 修改后台地区只能设置三级
9. 机构入驻流程优化
10.修复后台批阅试卷页面正确答案显示错误
11.修复积分兑换页面 兑换商品名称长度过长的显示问题
12.修复阿里云短信因为参数长度问题造成发送不成功的问题
13.修复签到开关按钮无效
14.新增腾讯云短信功能
15.修复后台一次不能添加多个会员等级

授权版更新
1. 商品上传报错问题验证
2. 修复浏览商品没有浏览记录的问题
3. 新增分享海报
4. 修复课程价格无法设置免费的问题
5. 修复域名是一级域名造成的微信登录的问题
6. 优化微信分享
7. 新增错题反馈功能
8. 修正个人中心浏览记录不显示
9. 修复机构首页不显示订单数量
10.虚拟订单详情新增店铺链接
11.优化机构入驻详细地址的定位
12.手机号登录注册去掉图片验证码


#### V2.2.1
DSKms免费版更新 
1. 优化开店入驻流程
2. 新增直播功能
3. 修改支付密钥隐藏
4. 修改代金券兑换需要的积分数据
5. 修改优化语言包
6. 修复平台充值卡全选删除无效
7. 新增分销商品获得佣金变量
8. 修复优化微信模板消息统一后台管理
9. 修复优化网站后台店铺动态评价


授权版更新
1. 优化开店入驻流程
2. 新增直播功能
3. 新增手机端关联版式
4. 新增手机端子账户权限控制

#### V2.2.0
DSKms免费版更新 
1. 自营机构去除机构登记
2. 去掉商品分类图片上传的限制
3. 机构首头部优化
4. 头部站内信息数量BUG修复
5. 新增支付宝/微信支付,不使用预存款和充值卡抵扣的情况下,支付原路款项原路退回。
6. 修复顶部快捷导航不显示的问题
7. 用户中心实名认证界面美化
8. 新增课程章节未免费且访问会员未购买过本课程情况下的章节按钮说明
9. 新增手机短信发送测试及错误信息提示、格式
10. 修复HTTPS网站使用微信登录无法生成二维码的问题
11. 修复视频观看页面 左上角的返回和收藏不生效的问题
12. 修复取消店铺收藏不会减少收藏数+添加用户时，
13. 新增默认的支付密码


授权版更新
1. 新增手机端店铺详情页面信息
2. 修改会员更换头像后 还是显示修改前头像的问题
3. 处理IOS手机 商品详情页面和商品分类页面滑动卡顿的问题
4. 修复手机端登录页面返回按钮失效的BUG
5. 新增微信模板消息功能


#### V2.1.9
DSKms免费版更新 
1. 后台上传按钮小优化
2. 去掉商品分类图片上传的限制
3. 机构首头部优化
4. 头部站内信息数量BUG修复
5. 新增支付宝/微信支付,不使用预存款和充值卡抵扣的情况下,支付原路款项原路退回。
6. 修复顶部快捷导航不显示的问题
7. 用户中心实名认证界面美化
8. 新增课程章节未免费且访问会员未购买过本课程情况下的章节按钮说明
9. 新增手机短信发送测试及错误信息提示、格式
10. 修复HTTPS网站使用微信登录无法生成二维码的问题
11. 修复视频观看页面 左上角的返回和收藏不生效的问题
12. 修复取消店铺收藏不会减少收藏数+添加用户时，
13. 新增默认的支付密码


授权版更新
1. 新增手机端店铺详情页面信息
2. 修改会员更换头像后 还是显示修改前头像的问题
3. 处理IOS手机 商品详情页面和商品分类页面滑动卡顿的问题
4. 修复手机端登录页面返回按钮失效的BUG
5. 新增微信模板消息功能

#### V2.1.8
DSKms免费版更新 
1. 优化分享图片功能
2. 支付方式优化，让显示更友好
3. 未登录时加入失败的提示
4. 修复聊天中包含商品时的样式
5. 一系列细节优化，提高用户体验
6. 修复代金券小BUG


授权版更新
1. 修复手机端禁止登录账户可正常登录
2. 修复当未添加手机端商品详情时，自动显示PC端商品详细
3. 修复手机端充值卡充值返回信息不一致


#### V2.1.7
DSKms免费版更新 
1. 优化分享二维码
2. 分销产品列表页美化
3. 广告图加上链接类型
4. 修改店铺中心 图片空间 删除图片没有提示信息和页面不跳转的BUG


#### V2.1.5
DSKms免费版更新 
1. 修复管理员权限菜单bug、美化列表页
2. 美化用户中心侧边栏界面
3. 新增整站推荐功能
4. 新增套餐设置为0元时，店铺可免费使用。
5. 修复html过滤后商品名称过长的提示
6. 新增后台编辑店铺排序
7. 修复店铺已关闭，店铺管理中心未有提示
8. 优化导航管理

授权版更新
1. 新增红包、大转盘、刮刮卡、砸金蛋、生肖翻翻看等平台活动


#### V2.1.3
DSKms免费版更新 
1. 修复图片水印无法正常显示
2. 统一使用阿里字体库
3. 修改会员中心样式显示错位
4. 发布商品添加事务处理
5. 仓库中的商品BUG修复
6. 修复163邮箱乱码问题
7. PC端买家中心/PC端卖家中心界面美化
8. 修复微信支付必须开启微信扫码支付
9. 修复用户中心通过缓存删除单条浏览记录

授权版更新
1. 手机端新增充值卡记录功能
2. 新增签到赠送积分

#### V2.1.2
DSKms授权版更新（ThinkPHP+VUEJS）
1.新增H5端卖家管理
2.优化用户绑定手机流程
3.卖家账户/买家账户同步登录
4.新增商品咨询

DSKms免费版更新 
1.优化用户绑定手机流程
2.卖家账户/买家账户同步登录
3.商品界面优化
4.部分界面美化


