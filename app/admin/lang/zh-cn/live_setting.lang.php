<?php
$lang['vod_tencent_play_key'] = '防盗链 Key';
$lang['vod_tencent_play_domain'] = '点播域名';
$lang['vod_tencent_appid'] = 'Tencent APPID';
$lang['vod_tencent_secret_id'] = 'Tencent SecretId';
$lang['vod_tencent_secret_key'] = 'Tencent SecretKey';
$lang['vod_tencent_secret_id_required'] = 'Tencent SecretId 为必填';
$lang['vod_tencent_secret_key_required'] = 'Tencent SecretKey 为必填';
$lang['vod_tencent_tips'] = '<a href="https://cloud.tencent.com/product/vod" target="_blank">相关文档</a>&nbsp;<a href="https://cloud.tencent.com/document/product/266/9219" target="_blank">申请密钥</a>';

$lang['live_push_domain'] = '推流域名';
$lang['live_push_key'] = '推流key';
$lang['live_play_domain'] = '拉流域名';
$lang['instant_message_verify'] = '直播聊天审核';
$lang['instant_message_gateway_url'] = '直播聊天gateway地址';
$lang['instant_message_register_url'] = '直播聊天register地址';
return $lang;