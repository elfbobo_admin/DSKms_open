<?php

/**
 * 机构视频管理
 */

namespace app\home\controller;
use think\facade\View;
use TencentCloud\Common\Credential;
use TencentCloud\Common\Profile\ClientProfile;
use TencentCloud\Common\Profile\HttpProfile;
use TencentCloud\Common\Exception\TencentCloudSDKException;
use TencentCloud\Vod\V20180717\VodClient;
use TencentCloud\Vod\V20180717\Models\DeleteMediaRequest;
use think\facade\Lang;
/**
 * ============================================================================
 * DSKMS多用户商城
 * ============================================================================
 * 版权所有 2014-2028 长沙德尚网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.csdeshang.com
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和使用 .
 * 不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * 控制器
 */
class  Sellervideo extends BaseSeller {

    public function initialize() {
        parent::initialize();
        Lang::load(base_path() . 'home/lang/' . config('lang.default_lang') . '/sellervideo.lang.php');
    }

    /**
     * 显示机构所有视频列表
     */
    public function index() {

        $videoupload_model = model('videoupload');
        $condition = array();
        $condition[] = array('store_id','=',session('store_id'));
        $videoupload_list = $videoupload_model->getVideouploadList($condition, '*', 10);


        View::assign('show_page', $videoupload_model->page_info->render());
        View::assign('videoupload_list', $videoupload_list);

        $this->setSellerCurItem('sellervideo_index');
        $this->setSellerCurMenu('sellervideo');
        return View::fetch($this->template_dir . 'index');
    }

    /**
     * 视频列表，外部调用
     */
    public function video_list(){
        $videoupload_model=model('videoupload');
        $video_list=$videoupload_model->getVideouploadList(array(array('store_id','=',session('store_id'))),'*',3);
     
        View::assign('video_list', $video_list);
        View::assign('show_page', $videoupload_model->page_info->render());
        echo View::fetch($this->template_dir . 'video_list');
    }
    /**
     * 获取腾讯客户端上传签名 
     * https://cloud.tencent.com/document/product/266/9221
     */
    public function getTencentSign() {
        // 确定 App 的云 API 密钥
        $secret_id = config('ds_config.vod_tencent_secret_id');
        $secret_key = config('ds_config.vod_tencent_secret_key');

        // 确定签名的当前时间和失效时间
        $current = TIMESTAMP;
        $expired = $current + 86400;  // 签名有效期：1天
        // 向参数列表填入参数
        $arg_list = array(
            "secretId" => $secret_id,
            "currentTimeStamp" => $current,
            "expireTime" => $expired,
            "random" => rand());

        // 计算签名
        $orignal = http_build_query($arg_list);
        $signature = base64_encode(hash_hmac('SHA1', $orignal, $secret_key, true) . $orignal);
        ds_json_encode(10000, '', $signature);
    }

    /**
     * 删除视频
     */
    public function del() {

        $videoupload_id = intval(input('param.id'));
        if ($videoupload_id <= 0) {
            ds_json_encode(10001, '参数错误');
        }
        $videoupload_model = model('videoupload');
        $condition = array();
        $condition[] = array('store_id','=',session('store_id'));
        $condition[] = array('videoupload_id','=',$videoupload_id);

        $videoupload = $videoupload_model->getOneVideoupload($condition);
        if (empty($videoupload)) {
            ds_json_encode(10001, '参数错误');
        }

        try {
            $cred = new Credential(config('ds_config.vod_tencent_secret_id'), config('ds_config.vod_tencent_secret_key'));
            $httpProfile = new HttpProfile();
            $httpProfile->setEndpoint("vod.tencentcloudapi.com");

            $clientProfile = new ClientProfile();
            $clientProfile->setHttpProfile($httpProfile);
            $client = new VodClient($cred, "", $clientProfile);

            $req = new DeleteMediaRequest();


            $params = '{"FileId":"' . $videoupload['videoupload_fileid'] . '"}';
            $req->fromJsonString($params);


            $resp = $client->DeleteMedia($req);

            $videoupload_model->delVideoUpload($condition);
        } catch (TencentCloudSDKException $e) {
            ds_json_encode(10001, $e->getMessage());
        }
        ds_json_encode(10000,lang('ds_common_del_succ'));
    }


    public function saveVideo() {
        $videoupload_fileid = input('param.file_id');
        $videoupload_url = input('param.url');
        $videoupload_type = intval(input('param.type'));
        $item_id = intval(input('param.item_id'));
        $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https' : 'http';
        if($http_type == 'https'){
        	$videoupload_url = str_replace('http', 'https', $videoupload_url);
        }
        if (empty($videoupload_fileid) || empty($videoupload_url)) {
            ds_json_encode(10001, '参数错误');
        }

        $videoupload_model = model('videoupload');

        $data = array(
            'videoupload_fileid' => $videoupload_fileid,
            'videoupload_url' => $videoupload_url,
            'videoupload_type' => $videoupload_type,
            'videoupload_time' => TIMESTAMP,
            'item_id' => $item_id,
            'store_id' => session('store_id'),
            'store_name' => session('store_name'),
        );
        $videoupload_model->addVideoupload($data);
         ds_json_encode(10000);
    }

    /**
     * 用户中心右边，小导航
     *
     * @param string $menu_type 导航类型
     * @param string $menu_key 当前导航的menu_key
     * @return
     */
    function getSellerItemList() {
        $menu_array = array(
            array(
                'name' => 'sellervideo_index',
                'text' => lang('sellervideo_index'),
                'url' => url('Sellervideo/index')
            )
        );
        return $menu_array;
    }

}
